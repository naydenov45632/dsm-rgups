import React from 'react';
import { Routes, Route } from "react-router-dom";
import Home from '../../pages/Home';
import { routes } from '../../routes';

function App() {
  return (
    <Routes>
        <Route path={routes.homeRoutes.root} element={<Home />} />
    </Routes>
  );
}

export default App;