/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import CallMark from '../../assets/CallMark'
import RgupsLogos, { eRgupsLogos } from '../../assets/RgupsLogos'
import SocialNetworks, { eSocialNetworks } from '../../assets/SocialNetworks'
import Span11 from '../Text/Span11'
import Span16Bold from '../Text/Span16Bold'
import styles from './styles.module.scss'

export default function Header() {
  return (
    <div className={styles.container}>
      <div className={styles.logos}>
        <a
          className={styles.link}
          href='https://www.rgups.ru/' 
          target='_blank' 
        >
          <RgupsLogos type={eRgupsLogos.rgups} />
        </a>
        <a
          className={styles.link}
          href='https://www.rgups.ru/university/struktura-i-organy-upravleniia-1632/fakultety/dorozhno-stroitel-nye-mashiny/' 
          target='_blank' 
        >
          <RgupsLogos type={eRgupsLogos.dsm} />
        </a>
      </div>
      <div className={styles.contacts}>
        <div className={styles.dean_numbers}>
          <CallMark />
          <div className={styles.dean_numbers__text}>    
            <a 
            className={styles.link}
            href="tel:+7(863)272-65-91"
            >
              <Span16Bold text='+7 (863) 272 65 91'/>
            </a>
            <Span11 className={styles.text} text='Работаем с 8:00 по 17:00'/>
          </div>
        </div>
        <div className={styles.social_networks}>
          <SocialNetworks type={eSocialNetworks.telegram} />
          <a
            className={styles.link}
            href='https://vk.com/fakultet_dsm_rgups' 
            target='_blank' 
          >
            <SocialNetworks type={eSocialNetworks.vkontakte} />
          </a>
          <a
            className={styles.link}
            href='https://instagram.com/dsm_rgups?igshid=YmMyMTA2M2Y=' 
            target='_blank' 
          >
            <SocialNetworks type={eSocialNetworks.instagram} />
          </a>
        </div>
      </div>
      <hr className={styles.hr}/>
    </div>
  )
}
