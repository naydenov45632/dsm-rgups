import React from 'react'
import styles from './styles.module.scss'

interface ICommonLayout {
    children: JSX.Element
}

export default function CommonLayout(props: ICommonLayout) {
    const { children } = props
  return (
    <div className={styles.container}>{children}</div>
  )
}
