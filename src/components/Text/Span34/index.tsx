import React from 'react'
import styles from './styles.module.scss'

interface ISpan34 {
    text: string;
}

export default function Span34(props: ISpan34) {
    const { text } = props
  return (
    <span className={styles.text}>{text}</span>
  )
}
