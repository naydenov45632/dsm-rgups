import React from 'react'
import styles from './styles.module.scss'

interface ISpan16Bold {
    text: string;
}

export default function Span16Bold(props: ISpan16Bold) {
    const { text } = props
  return (
    <span className={styles.text}>{text}</span>
  )
}
