import React from 'react'
import styles from './styles.module.scss'

interface ISpan11 {
    text: string;
    className?: string;
}

export default function Span11(props: ISpan11) {
    const { text, className } = props
  return (
    <span className={[styles.text, className].join(' ')}>{text}</span>
  )
}
