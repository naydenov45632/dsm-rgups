import React from 'react'
import styles from './styles.module.scss'

interface ISpan16 {
    text: string;
    className?: string;
}

export default function Span16(props: ISpan16) {
    const { text, className } = props
  return (
    <span className={[styles.text, className].join(' ')}>{text}</span>
  )
}
