import React from 'react'
import CheckMark from '../../assets/CheckMark'
import Span16 from '../Text/Span16'
import styles from './styles.module.scss'

interface IPlusesOfUniversity {
  text: string
}

export const plusesOfUniversityTexts = ["text1", "text2", "text3", "text4"]

export default function PlusesOfUniversity(props: IPlusesOfUniversity) {
    const { text } = props
  return (
    <div className={styles.container}>
      <CheckMark />
      <Span16 text={text}/>
    </div>
  )
}
