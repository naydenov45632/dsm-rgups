import React from 'react'
import Sponsors, { eSponsors } from '../../../../assets/Sponsors'
import CommonLayout from '../../../../components/Layouts/CommonLayout'
import styles from './styles.module.scss'

export default function SponsorsBlock() {
  return (
    <section>
        <CommonLayout>
            <div className={styles.container}>
                <Sponsors type={eSponsors.RosIntech} />
                <Sponsors type={eSponsors.KeyAuto} />
                <Sponsors type={eSponsors.RZD} />
                <Sponsors type={eSponsors.HB} />
                <Sponsors type={eSponsors.Rostselmach} />
            </div>
        </CommonLayout>
    </section>
  )
}
