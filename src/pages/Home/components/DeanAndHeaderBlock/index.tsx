import React from 'react'
import BranchDeanPhoto from '../../../../assets/BranchDean'
import UniversityBackground from '../../../../assets/UniversityBackground'
import WhyWeAreBackground from '../../../../assets/WhyWeAreBackground'
import Header from '../../../../components/Header'
import CommonLayout from '../../../../components/Layouts/CommonLayout'
import styles from './styles.module.scss'

export default function DeanAndHeaderBlock() {
  return (
    <section className={styles.section}>
      <div className={styles.image__container}>
        <UniversityBackground />
      </div>
      <div className={styles.background}>
              <WhyWeAreBackground />
            </div>
      <CommonLayout>
        <div>
          <Header />
          <div className={styles.dean_block__container}>
            <div className={styles.dean_block__image}>
              <BranchDeanPhoto />
            </div>

          </div>
        </div>
      </CommonLayout>
    </section>
  );
}
