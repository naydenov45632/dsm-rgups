import React from 'react'
import CommonLayout from '../../../../components/Layouts/CommonLayout'
import PlusesOfUniversity, { plusesOfUniversityTexts } from '../../../../components/PlusesOfUniversity'
import Span16 from '../../../../components/Text/Span16'
import Span34 from '../../../../components/Text/Span34'
import styles from './styles.module.scss'

export default function WhyAreWe() {

  const mapPlusesOfUniversity = React.useMemo(() => {
    return plusesOfUniversityTexts.map((pluses, index) => {
      return <PlusesOfUniversity text={pluses} key={index}/>
    })
  }, [])

  return (
    <section className={styles.container}>
        <CommonLayout>
          <div className={styles.wrapper}>
            <div className={styles.title}>
              <div className={styles.title__container}>
                <Span34 text='Почему мы?'/>
                <Span16 className={styles.title__description} text='Один из лучших вузов Ростова-на-Дону'/>
              </div>
            </div>
            <div className={styles.pluses__container}>
                {mapPlusesOfUniversity}
              </div>
          </div>
        </CommonLayout>
    </section>
  )
}
