import React from 'react'
import DeanAndHeaderBlock from './components/DeanAndHeaderBlock'
import SponsorsBlock from './components/SponsorsBlock'
import WhyAreWe from './components/WhyAreWe'

export default function Home() {
  return (
    <>
      <DeanAndHeaderBlock />
      <WhyAreWe />
      <SponsorsBlock />
    </>
  )
}
