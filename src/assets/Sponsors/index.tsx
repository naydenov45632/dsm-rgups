import React from 'react'
import HB from './HB.svg';
import KeyAuto from './KeyAuto.svg';
import Rostselmach from './Rostselmach.svg';
import RosIntech from './RosIntech.svg';
import RZD from './RZD.svg';

export enum eSponsors {
    HB = "HB",
    KeyAuto = "KeyAuto",
    RosIntech = "RosIntech",
    Rostselmach = "Rostselmach",
    RZD = "RZD",
}

interface ISponsorsProps {
    type: eSponsors;
}

export default function Sponsors(props: ISponsorsProps) {
    const { type } = props

    const handleChooseSponsor = (sponsorType: eSponsors) => {
      if (sponsorType === eSponsors.HB) {
        return (
            <img src={HB} alt='HB' />
        );
      }
      if (sponsorType === eSponsors.KeyAuto) {
        return (
            <img src={KeyAuto} alt='KeyAuto' />
        );
      }
      if (sponsorType === eSponsors.RosIntech) {
        return (
            <img src={RosIntech} alt='RosIntech' />
        );
      }
      if (sponsorType === eSponsors.Rostselmach) {
        return (
            <img src={Rostselmach} alt='Rostselmach' />
        );
      }
      if (sponsorType === eSponsors.RZD) {
        return (
            <img src={RZD} alt='RZD' />
        );
      }
    };

    return (
        <>
            {handleChooseSponsor(type)}
        </>
    )
}
