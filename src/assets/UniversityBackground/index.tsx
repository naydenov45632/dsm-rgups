import React from 'react'
import University from './University.png';
import styles from './styles.module.scss'

export default function UniversityBackground() {
    return (
        <img className={styles.image} src={University} alt="University"/>
      )
}
