import React from 'react'
import Dsm from './Dsm.svg';
import DsmBig from './DsmBig.svg';
import Rgups from './Rgups.svg';
import RgupsBig from './RgupsBig.svg';

export enum eRgupsLogos {
    dsm = "dsm",
    dsmBig = "dsmBig",
    rgups = "rgups",
    rgupsBig = "rgupsBig",
}

interface ISocialNetworksProps {
    type: eRgupsLogos;
}

export default function RgupsLogos(props: ISocialNetworksProps) {
    const { type } = props

    const handleChooseLogo = (rgupsLogoType: eRgupsLogos) => {
      if (rgupsLogoType === eRgupsLogos.dsmBig) {
        return (
            <img src={DsmBig} alt='DsmBig' />
        );
      }
      if (rgupsLogoType === eRgupsLogos.dsm) {
        return (
            <img src={Dsm} alt='Dsm' />
        );
      }
      if (rgupsLogoType === eRgupsLogos.rgups) {
        return (
            <img src={Rgups} alt='Rgups' />
        );
      }
      if (rgupsLogoType === eRgupsLogos.rgupsBig) {
        return (
            <img src={RgupsBig} alt='RgupsBig' />
        );
      }
    };

    return (
        <>
            {handleChooseLogo(type)}
        </>
    )
}
