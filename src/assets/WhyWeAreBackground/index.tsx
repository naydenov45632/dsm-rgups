import React from "react";

export default function WhyWeAreBackground() {
  return (
    <svg
      width="1442"
      height="101"
      viewBox="0 0 1442 101"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <ellipse cx="720" cy="101" rx="767" ry="101" fill="#F9F7EE" />
    </svg>
  );
}
