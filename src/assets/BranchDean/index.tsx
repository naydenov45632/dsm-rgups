import React from 'react'
import BranchDean from './BranchDean.png';

export default function BranchDeanPhoto() {
  return (
    <img src={BranchDean} alt="BranchDean"/>
  )
}
