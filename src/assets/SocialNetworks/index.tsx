import React from 'react'
import Instagram from './Instagram.svg';
import Telegram from './Telegram.svg';
import Vkontakte from './Vkontakte.svg';

export enum eSocialNetworks {
    instagram = "instagram",
    vkontakte = "vkontakte",
    telegram = "telegram"
}

interface ISocialNetworksProps {
    type: eSocialNetworks;
}

export default function SocialNetworks(props: ISocialNetworksProps) {
    const { type } = props

    const handleChooseSocialNetwork = (netWorkType: eSocialNetworks) => {
      if (netWorkType === eSocialNetworks.instagram) {
        return (
            <img src={Instagram} alt='Instagram' />
        );
      }
      if (netWorkType === eSocialNetworks.vkontakte) {
        return (
            <img src={Vkontakte} alt='Vkontakte' />
        );
      }
      if (netWorkType === eSocialNetworks.telegram) {
        return (
            <img src={Telegram} alt='Telegram' />
        );
      }
    };

    return (
        <>
            {handleChooseSocialNetwork(type)}
        </>
    )
}
